package com.test.util;

import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class FileWordOccurCountReaderTest {

    private FileWordOccurCountReader instance = new FileWordOccurCountReader("input.txt");

    @Test
    public void shouldExtractEmptyWordCountMapForGivenEmptyLines() throws IOException {
        List<String> lines = new ArrayList<>();
        instance = spy(instance);
        doReturn(lines).when(instance).getStreamOfLinesFromFile();

        Map<String, Long> wordCountMap = instance.extractWordCountMap();

        assertTrue(wordCountMap.isEmpty());
    }

    @Test
    public void shouldExtractWordCountMapForGivenLines() throws IOException {
        List<String> lines = new ArrayList<>();
        lines.add("A A B C");
        lines.add("H B  A");
        instance = spy(instance);
        doReturn(lines).when(instance).getStreamOfLinesFromFile();

        Map<String, Long> wordCountMap = instance.extractWordCountMap();

        assertEquals(4L, wordCountMap.size());
        assertEquals(Long.valueOf(3L), wordCountMap.get("A"));
        assertEquals(Long.valueOf(2L), wordCountMap.get("B"));
        assertEquals(Long.valueOf(1L), wordCountMap.get("C"));
        assertEquals(Long.valueOf(1L), wordCountMap.get("H"));
    }
}

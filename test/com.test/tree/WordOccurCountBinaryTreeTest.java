package com.test.tree;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class WordOccurCountBinaryTreeTest {

    private WordOccurCountBinaryTree instance = new WordOccurCountBinaryTree();

    @Test
    public void shouldReturnNullRootRightAfterCreation() {
        assertNull(instance.getRoot());
    }

    @Test
    public void shouldInsertFirstNodeAndReturnItAsRoot() {
        WordOccurNode givenNode = new WordOccurNode("word", 1L);

        instance.insert(givenNode);
        WordOccurNode actualNode = (WordOccurNode) instance.getRoot();

        assertEquals(givenNode, actualNode);
        assertNull(actualNode.getLeft());
        assertNull(actualNode.getRight());
    }

    @Test
    public void shouldRebuildTreeIfSecondNodeAddedAndPutFirstAtLeftAndSecondAtRight() {
        WordOccurNode rootNode = new WordOccurNode("wordLeft", 1L);
        WordOccurNode secondNode = new WordOccurNode("wordRight", 2L);
        AccumulateNode expectedRootNode = new AccumulateNode(rootNode, secondNode, 1L, 3L);
        instance.insert(rootNode);

        instance.insert(secondNode);

        AccumulateNode actualRootNode = instance.getRoot();
        AccumulateNode actualLeft = actualRootNode.getLeft();
        AccumulateNode actualRight = actualRootNode.getRight();
        assertEquals(expectedRootNode, actualRootNode);
        assertEquals(rootNode, actualLeft);
        assertEquals(secondNode, actualRight);
        assertTrue(actualLeft.left == null && actualLeft.right == null);
        assertTrue(actualRight.left == null && actualRight.right == null);
    }

    @Test
    public void shouldInsertRecursivelyWithDepthOneThenCheckItWasInvokedTwoTimes() {
        WordOccurNode wordOccurNodeA = new WordOccurNode("A", 1L);
        WordOccurNode wordOccurNodeB = new WordOccurNode("B", 2L);
        WordOccurNode wordOccurNodeC = new WordOccurNode("C", 3L);
        instance.insert(wordOccurNodeA);
        instance.insert(wordOccurNodeB);
        AccumulateNode root = instance.getRoot();

        instance = spy(instance);
        instance.insertRecursively(root, wordOccurNodeC);

        verify(instance, times(2)).insertRecursively(anyObject(), anyObject());
        assertEquals(instance.getRoot().getLeft(), wordOccurNodeA);
        assertEquals(instance.getRoot().getRight().getRight(), wordOccurNodeC);
        assertEquals(instance.getRoot().getRight().getLeft(), wordOccurNodeB);
    }

    @Test
    public void shouldSplitGivenNodeAndCreateSubtreeOfThreeNodesWhereRootIsAccumulateNode() {
        WordOccurNode givenNode = new WordOccurNode("word", 1L);
        WordOccurNode newNode = new WordOccurNode("secondWord", 4L);

        AccumulateNode newRoot = instance.splitNode(givenNode, newNode);

        assertTrue(newRoot.getClass() == AccumulateNode.class);
        AccumulateNode actualLeft = newRoot.getLeft();
        AccumulateNode actualRight = newRoot.getRight();
        assertEquals(actualLeft, givenNode);
        assertEquals(actualRight, newNode);
        assertTrue(actualLeft.left == null && actualLeft.right == null);
        assertTrue(actualRight.left == null && actualRight.right == null);
    }
}

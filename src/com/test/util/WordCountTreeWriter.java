package com.test.util;

import com.test.tree.WordOccurCountBinaryTree;

public interface WordCountTreeWriter {
    void write(WordOccurCountBinaryTree tree);
}

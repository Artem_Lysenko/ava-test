package com.test.util;

import java.util.Map;

public interface WordCountReader {
    Map<String, Long> read();
}

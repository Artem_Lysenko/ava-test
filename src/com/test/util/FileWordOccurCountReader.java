package com.test.util;

import com.test.exception.ApplicationException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

public class FileWordOccurCountReader implements WordCountReader {

    private String fileName;

    public FileWordOccurCountReader(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Map<String, Long> read() {
        try {
            return extractWordCountMap();
        } catch (IOException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    protected Map<String, Long> extractWordCountMap() throws IOException {
        return getStreamOfLinesFromFile()
                .stream()
                .map(line -> line.split("[\\s]+"))
                .flatMap(Arrays::stream)
                .collect(Collectors.groupingBy(Function.identity(), counting()));
    }

    protected List<String> getStreamOfLinesFromFile() throws IOException {
        return Files.readAllLines(Paths.get(fileName));
    }
}

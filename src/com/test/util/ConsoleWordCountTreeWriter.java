package com.test.util;

import com.test.exception.ApplicationException;
import com.test.tree.AccumulateNode;
import com.test.tree.WordOccurCountBinaryTree;
import com.test.tree.WordOccurNode;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class ConsoleWordCountTreeWriter implements WordCountTreeWriter {

    @Override
    public void write(WordOccurCountBinaryTree tree) {
        try (OutputStreamWriter osw = new OutputStreamWriter(System.out)) {
            printTree(tree, osw);
        } catch (IOException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    protected void printTree(WordOccurCountBinaryTree binaryTree, OutputStreamWriter out) throws IOException {
        if (binaryTree.getRoot() == null) {
            out.write("<null>");
        } else {
            printTree(binaryTree.getRoot(), out);
        }
    }

    protected void printTree(AccumulateNode node, OutputStreamWriter out) throws IOException {
        AccumulateNode right = node.getRight();
        AccumulateNode left = node.getLeft();

        if (right != null) {
            printTree(right, out, true, "");
        }
        printNodeValue(node, out);
        if (left != null) {
            printTree(left, out, false, "");
        }
    }

    private void printNodeValue(AccumulateNode node, OutputStreamWriter out) throws IOException {
        if (node.getLeft() != null) {
            out.write(node.getTotalOccurrenceCount().toString());
        } else {
            out.write(((WordOccurNode)node).getWord());
            out.write(" (");
            out.write(node.getOccurrenceCount().toString());
            out.write(")");
        }
        out.write('\n');
    }

    private void printTree(AccumulateNode node, OutputStreamWriter out, boolean isRight, String indent) throws IOException {
        AccumulateNode right = node.getRight();
        AccumulateNode left = node.getLeft();

        if (right != null) {
            printTree(right, out, true, indent + (isRight ? "        " : " |      "));
        }
        out.write(indent);
        if (isRight) {
            out.write(" /");
        } else {
            out.write(" \\");
        }
        out.write("----- ");
        printNodeValue(node, out);
        if (left != null) {
            printTree(left, out, false, indent + (isRight ? " |      " : "        "));
        }
    }
}

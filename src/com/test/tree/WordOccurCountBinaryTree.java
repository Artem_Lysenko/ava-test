package com.test.tree;

public class WordOccurCountBinaryTree {

    private AccumulateNode root;

    public void insert(AccumulateNode node) {
        if (null == root) {
            root = node;
        } else {
            root = insertRecursively(root, node);
        }
    }

    protected AccumulateNode insertRecursively(AccumulateNode current, AccumulateNode node) {
        AccumulateNode outcomeNode = current;

        if (current.left != null) {
            if (node.occurrenceCount <= current.occurrenceCount) {
                current.left = insertRecursively(current.left, node);
            } else {
                current.right = insertRecursively(current.right, node);
            }
            current.totalOccurrenceCount += node.occurrenceCount;
        } else {
            outcomeNode = splitNode(current, node);
        }

        return outcomeNode;
    }

    protected AccumulateNode splitNode(AccumulateNode current, AccumulateNode node) {

        AccumulateNode newNode = new AccumulateNode();
        newNode.occurrenceCount = current.occurrenceCount;
        newNode.totalOccurrenceCount = node.occurrenceCount + newNode.occurrenceCount;

        if (node.occurrenceCount <= current.occurrenceCount) {
            newNode.left = node;
            newNode.right = current;
        } else {
            newNode.right = node;
            newNode.left = current;
        }

        return newNode;
    }

    public AccumulateNode getRoot() {
        return root;
    }
}

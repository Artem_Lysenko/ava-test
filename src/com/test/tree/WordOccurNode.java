package com.test.tree;

import java.util.Objects;

public class WordOccurNode extends AccumulateNode {

    String word;

    public WordOccurNode(String word, Long occurrenceCount) {
        this.occurrenceCount = occurrenceCount;
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof WordOccurNode)) return false;

        if (!super.equals(o)) return false;

        WordOccurNode that = (WordOccurNode) o;
        return Objects.equals(word, that.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), word);
    }
}

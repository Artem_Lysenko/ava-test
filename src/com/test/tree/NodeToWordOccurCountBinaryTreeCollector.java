package com.test.tree;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class NodeToWordOccurCountBinaryTreeCollector implements Collector<AccumulateNode,
        List<AccumulateNode>, WordOccurCountBinaryTree> {

    @Override
    public Supplier<List<AccumulateNode>> supplier() {
        return ArrayList::new;
    }

    @Override
    public BiConsumer<List<AccumulateNode>, AccumulateNode> accumulator() {
        return List::add;
    }

    @Override
    public BinaryOperator<List<AccumulateNode>> combiner() {
        return (acc, nodes) -> {
            acc.addAll(nodes);
            return acc;
        };
    }

    @Override
    public Function<List<AccumulateNode>, WordOccurCountBinaryTree> finisher() {

        return (nodes) -> {

            WordOccurCountBinaryTree tree = new WordOccurCountBinaryTree();
            for (AccumulateNode node : nodes) {
                tree.insert(node);
            }
            return tree;
        };
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.UNORDERED));
    }
}


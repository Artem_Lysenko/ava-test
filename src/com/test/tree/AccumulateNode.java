package com.test.tree;

import java.util.Objects;

public class AccumulateNode {

    Long occurrenceCount;
    Long totalOccurrenceCount;

    AccumulateNode left;
    AccumulateNode right;

    public AccumulateNode() {
    }

    public AccumulateNode(AccumulateNode left, AccumulateNode right, Long occurrenceCount, Long totalOccurrenceCount) {
        this.occurrenceCount = occurrenceCount;
        this.totalOccurrenceCount = totalOccurrenceCount;
        this.left = left;
        this.right = right;
    }

    public Long getOccurrenceCount() {
        return occurrenceCount;
    }

    public Long getTotalOccurrenceCount() {
        return totalOccurrenceCount;
    }

    public AccumulateNode getLeft() {
        return left;
    }

    public AccumulateNode getRight() {
        return right;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;
        AccumulateNode that = (AccumulateNode) o;

        return occurrenceCount.equals(that.occurrenceCount) &&
                totalOccurrenceCount.equals(that.totalOccurrenceCount) &&
                left == that.left &&
                right == that.right;
    }

    @Override
    public int hashCode() {
        return Objects.hash(occurrenceCount, totalOccurrenceCount, left, right);
    }
}

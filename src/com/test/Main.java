package com.test;

import com.test.exception.ApplicationException;

public class Main {

    public static void main(String[] args) {
        boolean isTraceEnabled = args.length > 0 && args[0].equals("-x");
        try {
            new ApplicationRunner().run();

        } catch (ApplicationException e) {
            System.out.println("Error: " + e.getMessage());

            if (isTraceEnabled) {
                e.printStackTrace();
            }
        }

    }
}

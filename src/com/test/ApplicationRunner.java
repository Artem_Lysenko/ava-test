package com.test;

import com.test.tree.NodeToWordOccurCountBinaryTreeCollector;
import com.test.tree.WordOccurCountBinaryTree;
import com.test.tree.WordOccurNode;
import com.test.util.ConsoleWordCountTreeWriter;
import com.test.util.FileWordOccurCountReader;
import com.test.util.WordCountReader;
import com.test.util.WordCountTreeWriter;

public class ApplicationRunner {

    public static final String DEFAULT_INPUT_FILENAME = "input.txt";

    public void run() {
        WordCountReader wordCountReader = new FileWordOccurCountReader(DEFAULT_INPUT_FILENAME);

        WordOccurCountBinaryTree tree = wordCountReader.read().entrySet()
                .stream()
                .map(entry -> new WordOccurNode(entry.getKey(), entry.getValue()))
                .collect(new NodeToWordOccurCountBinaryTreeCollector());

        WordCountTreeWriter treeWriter = new ConsoleWordCountTreeWriter();
        treeWriter.write(tree);
    }
}
